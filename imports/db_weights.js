import {
  Mongo
}
from 'meteor/mongo';

export const ResidentWeights = new Mongo.Collection('resident_weights');
