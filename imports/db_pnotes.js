import {
  Mongo
}
from 'meteor/mongo';

export const ProgressNotes = new Mongo.Collection('pnotes');
