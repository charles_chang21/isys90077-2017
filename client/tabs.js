import {
  Template
}
from 'meteor/templating';
import {
  Session
}
from 'meteor/session';

Template.tabsTemplate.helpers({
  residentsTab: function() {
    if (Session.get("tab") == "residents")
      return "active";
    else
      return "";
  },
  assessmentsTab: function() {
    if (Session.get("tab") == "assessments")
      return "active";
    else
      return "";
  },
  carePlanTab: function() {
    if (Session.get("tab") == "careplan")
      return "active";
    else
      return "";
  },
  pnotesTab: function() {
    if (Session.get("tab") == "pnotes")
      return "active";
    else
      return "";
  },
  chartsTab: function() {
    if (Session.get("tab") == "charts")
      return "active";
    else
      return "";
  },

});


Template.tabsTemplate.events({
  'click #residents': function(e, instance) {
    e.preventDefault();
    Session.set('tab', "residents");
    Session.set('page', "residentListTemplate");
  },
  'click #assessments': function(e, instance) {
    e.preventDefault();
    Session.set('tab', "assessments");
    Session.set('page', "assessmentsTemplate");
  },
  'click #careplan': function(e, instance) {
    e.preventDefault();
    Session.set('tab', "careplan");
    Session.set('page', "careplanTemplate");
  },
  'click #pnotes': function(e, instance) {
    e.preventDefault();
    Session.set('tab', "pnotes");
    Session.set('page', "pnotesTemplate");
  },
  'click #charts': function(e, instance) {
    e.preventDefault();
    Session.set('tab', "charts");
    Session.set('page', "chartsTemplate");
  },
});
