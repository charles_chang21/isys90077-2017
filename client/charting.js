import {
  Template
}
from 'meteor/templating';
import {
  Residents
}
from '/imports/db';
import {
  ResidentWeights
}
from '/imports/db_weights';


Template.chartsTemplate.helpers({
  residents: function() {
    return Residents.find({});
  }

});


Template.chartsTemplate.events({
  'click #openWeightChart': function(e, instance) {
    e.preventDefault();
    const residentId = e.toElement.getAttribute("residentId");

    Session.set('page', "residentWeightChart");
    Session.set('residentId', residentId);
  }
});
