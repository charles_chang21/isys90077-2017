import {
  Residents
}
from '/imports/db';

import {
  ResidentWeights
}
from '/imports/db_weights';

Template.residentWeightChart.helpers({

  residents: function() {
    const residentId = Session.get('residentId');
    return Residents.find({
      _id: residentId
    });
  },
  weights: function() {
    const residentId = Session.get('residentId');
    return ResidentWeights.find({
      ResidentID: residentId
    });
  }

});

Template.residentWeightChart.events({
  'click #charting': function(e, instance) {
    e.preventDefault();
    Session.set('page', "chartsTemplate");
  },
  'click #save_weight': function(e, instance) {
    e.preventDefault();
    const weight = $('#weight').val();
    const recordDate = $('#record_date').val();
    const residentId = Session.get('residentId');

    ResidentWeights.insert({
      ResidentID: residentId,
      Weight: weight,
      RecordDate: recordDate,
      createdAt: new Date(),
    });
  }
});

Template.residentWeightChart.rendered = function() {
  $('#record_date').datepicker({
    format: 'dd/mm/yyyy'
  });
}
