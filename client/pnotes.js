import {
  Template
}
from 'meteor/templating';
import {
  Residents
}
from '/imports/db';

import {
  ProgressNotes
}
from '/imports/db_pnotes';

Template.pnotesTemplate.helpers({
  progressNotes: function() {
    return ProgressNotes.find({}, {
      sort: {
        "createdAt": -1
      },
      limit: 5
    });
  }
});

Template.pnotesTemplate.events({
  'click #addnote': function(e, instance) {
    e.preventDefault();
    Session.set('page', "pnotesAddTemplate");
  },
  'click #delnote': function(e, instance) {
    e.preventDefault();
    const residentID = e.toElement.getAttribute("residentID");
    if (confirm('Are you sure you want to remove this progress note?')) {
      ProgressNotes.remove({
        _id: residentID
      });
    }

  }

});

Template.pnotesAddTemplate.helpers({
  residents: function() {
    return Residents.find({});
  }
});

Template.pnotesAddTemplate.events({
  'click #listnote': function(e, instance) {
    Session.set('page', "pnotesTemplate");
  },
  'click #save': function(e, instance) {
    e.preventDefault();

    const residentID = $('#resident').val();
    const residentName = $("#resident option:selected").text();
    const note = CKEDITOR.instances['notes'].getData();

    ProgressNotes.insert({
      ResidentID: residentID,
      ResidentName: residentName,
      Notes: note,
      createdAt: new Date(),
    });

    CKEDITOR.instances['notes'].setData('');

    Session.set('page', "pnotesTemplate");
  }
});
