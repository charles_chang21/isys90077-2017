import {
  ResidentWeights
}
from '/imports/db_weights';

Template.weightChart.topGenresChart = function() {

  const residentId = Session.get('residentId');
  const weights = ResidentWeights.find({
    ResidentID: residentId
  }).fetch();

  var dataSeries = [];
  for (var i = 0; i < weights.length; i++) {
    var w = parseInt(weights[i].Weight);
    dataSeries.push([w]);
  }

  return {
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: 'Weight Chart'
    },
    tooltip: {
      pointFormat: '<b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.value}',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
              'black'
          },
          connectorColor: 'silver'
        }
      }
    },
    series: [{
      type: 'line',
      name: 'Time',
      data: dataSeries
    }]
  };
};
