import {
  Meteor
}
from 'meteor/meteor';
import {
  Residents
}
from '/imports/db';
import {
  ProgressNotes
}
from '/imports/db_pnotes';

import {
  ResidentWeights
}
from '/imports/db_weights';

Meteor.startup(() => {
  // code to run on server at startup
});
